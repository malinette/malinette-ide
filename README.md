# La Malinette - IDE
A Free Open Source Kit To Simplify Programming Interactivity

- Version: 2.1
- Languages: English, French, Spanish (for interface, examples and manuals)
- Authors: Reso-nance Numérique, Jérôme Abel
- Contact: contact_at_reso-nance.org
- Website: http://malinette.info
- Development: http://framagit.org/malinette
- Licence: GNU/GPL v3

Special thanks for all Pure Data developers. We've taken some good ideas from few great projects (pdmtl abstractions, DIY2, rjdj, ...).

## Description
Malinette is an all-in-one solution, a rapid prototyping tool to show and make simple interaction and multimedia projects. It is a software and a hardware set. The software is based on Pure Data Vanilla. It is also a very convenient suitcase and wood boxes with sensors, actuators, Arduino, Teensy and electronic components to test ideas and projects very quickly.

## Architecture
The Malinette software is divided in three parts from newbies to advanced users:
- [malinette-soft](https://framagit.org/malinette/malinette-soft): the standalone way to use the Malinette, a single folder containing Pure Data, externals, malinette and a startup script;
- [malinette-ide](https://framagit.org/malinette/malinette-ide): a framework and an interface to learn programming and rapid prototyping;
- [malinette-abs](https://framagit.org/malinette/malinette-abs): a set of abstractions to use Pure Data easily;

## Other Resources
- BrutBoX. Note that the software is also used for the [BrutBox](https://git.framasoft.org/resonance/brutbox/) musical interface.
- Hardware. You will find also a [malinette-hw](https://framagit.org/malinette/malinette-hw) repository that contains hardware designs.
- Documentations. [malinette-docs](https://framagit.org/malinette/malinette-docs)

## Requirements
- Computer: a decent computer (>2008 is adviced)
- Operating system: should work on Mac OS X, MS Windows and main GNU/Linux distributions.
- Pure Data: >0.51 version
- Dependencies (externals): bassemu~ comport cyclone ext13 Gem ggee hcs hid (or hidin for windows) iemguts iemlib list-abs mapping maxlib moocow pix_fiducialtrack pmpd puremapping purepd sigpack tof zexy

See the "INSTALL.md" file for installation advices.

## Arduino
If you want to use an Arduino, download the [Arduino-IDE software](http://www.arduino.cc/en/Main/Software).

Plug the USB board, install drivers if required, and upload the "StandardFirmata" sketch from the Arduino software : Menu File > Examples > Firmata > StandardFirmata

On Windows 10 or Mac OS X, we note that the Arduino/Leonardo board and Firmata does not work with [pduino]. So you should convert it in MIDI (see [malinette-hw](https://framagit.org/malinette/malinette-hw)) or use a Arduino/UNO board for instance.

Note: For Windows 8, disable Driver Signature Verification.

## Updates
Few malinette-ide folders could be stored in the same folder if they don't have the same name. Keep in mind that is a good practice to rename old "malinette-ide" to keep your projects and data.

So, for malinette-soft users, you can keep your old malinette-ide folder and pick a new one to be up-to-date.

## Configurations

### Resize patchs
Sometimes, depends on operating system, the menu and patches don't show as you wish. After copy a backup of "malinette-ide", you can resize all patches with the "tools/resize_patches.sh" script. Edit it to find your specific dimensions.

### Preferences
In the "preferences/preferences.txt" file you can change the lang of the interface, the port of an Arduino, the screen size of video rendering. We added extra features like EDIT to not allow edit mode on the menu, and 10BITS to allow 10 bits sensor for BrutBox Midi interface.

Another setup could be to change the list of objects, it is possible by editing the "preferences/malinette-objects-list.txt".

## User instructions
- Open the "MALINETTE.pd" for the standard version or the "BRUTBOX.pd" for the Brutbox version.
- Open Manual examples to see how it works

You have two windows : the menu on the left and the project window on the right (called a "patch" in Pure Data). Basically, you can open examples or create your project. Clic on the "?" buttons to find all objects of a category. You can also find some documentations in the "./docs" folder.

To start a project, first way is to open the "new" patch, fill it with some objects, and save it with an another name. A more rich way is to go to "malinette-ide/projects", copy and paste the "malinette-default", and rename it. Next, go to "malinette-ide/preferences" and edit "preferences.txt" file with a text editor (on windows, notepad seems better that bloc notes). Change the "malinette-folder: malinette-default" by "malinette-folder: my-projects" for instance. Next time you will open MALINETTE.pd it will show you the contents of "my-projects". The structure of a project folder must be :
- my-projects/new.pd: the start patch
- .........../data: to store data, presets, medias, ...
- .........../abs: to store your abstractions

## Content
- ./abs    			: all objects (audio, in, numbers, out, seq, video)
- ./docs            : images for documentation (see [malinette-docs](https://framagit.org/malinette/malinette-docs) for objects and exemples documentations)
- ./examples        : examples and manual patches, also some processing examples
- ./preferences     : preferences file, language translations, logos and dependencies
- ./projects        : your projects folders
- ./tools         	: scripts to make helps, download packages, tclplugins, ..
- MALINETTE.pd      : open this patch to start the ide
- BRUTBOX.pd      	: beginner version with BrutBox MIDI interface

## Files formats (video, audio, 3d etc..)
- Video : best format for video files (better performance in pd) is MJPEG, this compression is better packed in .avi for windows and .mov for mac.
- Audio : Pure Data audio objects use .aiff or .wav, except some librairies which use mp3 (we do not have mp3 player yet).
- 3d: .obj is the 3d-model-file you can import in Pure Data, you can create .obj from most of 3D software (like Blender).

## Some documentations
- Pure Data and Arduino: http://flossmanuals.net/ (french and english)
- French forum: http://codelab.fr/
- English Pure Data forum: https://forum.pdpatchrepo.info/
- Youtube channel : https://www.youtube.com/channel/UCqd_bN13_IeVkXg83jLTTWg
- Pd-list: https://lists.puredata.info/pipermail/pd-list/
